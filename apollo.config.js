module.exports = {
  client: {
    service: {
      name: 'backend-yoga',
      localSchemaFile: '../backend-yoga/src/schema/schema.graphql'
    }
  }
};
